package data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import logic.Benutzer;
import logic.Voting;

public class Data {
	public Data() {}

	public void benutzer_erstellen(String name1, String stamm1) {
		// Parameter fuer Verbindungsaufbau definieren
		String driver = "com.mysql.jdbc.Driver";
		String url = "jdbc:mysql://localhost/?";
		String user = "root";
		String password = "";
		try {
			// JDBC-Treiber laden
			Class.forName(driver);
			// Verbindung aufbauen
			Connection con;
			con = DriverManager.getConnection(url, user, password);
			// SQL-Anweisungen ausfuehren
			Statement stmt = con.createStatement();
			stmt.execute("INSERT INTO T_Person VALUES (" + name1 + "," + stamm1 + ")");
			con.close();
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();

		}
	}
	
	public void benutzer_abrufen(String name1) {
		// Parameter fuer Verbindungsaufbau definieren
		String driver = "com.mysql.jdbc.Driver";
		String url = "jdbc:mysql://localhost/rechtecke?";
		String user = "root";
		String password = "";

		try {
			// JDBC-Treiber laden
			Class.forName(driver);
			// Verbindung aufbauen
			Connection con;
			con = DriverManager.getConnection(url, user, password);
			// SQL-Anweisungen ausfuehren
			Statement stmt = con.createStatement();
			stmt.execute("SELECT p_Name FROM T_Person WHERE p_Name=%" + name1 + "%;");
			ResultSet rs = stmt.executeQuery("SELECT p_Name FROM T_Person WHERE p_Name=%" + name1 + "%;");
			con.close();
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();

		}
		
	}
	
	public void song_erstellen(String titel1 , String bandname1, String genre1, String f_Name1) {
		// Parameter fuer Verbindungsaufbau definieren
		String driver = "com.mysql.jdbc.Driver";
		String url = "jdbc:mysql://localhost/?";
		String user = "root";
		String password = "";
		try {
			// JDBC-Treiber laden
			Class.forName(driver);
			// Verbindung aufbauen
			Connection con;
			con = DriverManager.getConnection(url, user, password);
			// SQL-Anweisungen ausfuehren
			Statement stmt = con.createStatement();
			stmt.execute("INSERT INTO T_Song VALUES (" + titel1 + "," + bandname1 + "," + genre1 + "," + f_Name1 + ")");
			con.close();
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();

		}
	}
	
	public void vote_erstellen(String titel1 , String bandname1, String genre1, String f_Name1) {
		// Parameter fuer Verbindungsaufbau definieren
		String driver = "com.mysql.jdbc.Driver";
		String url = "jdbc:mysql://localhost/?";
		String user = "root";
		String password = "";
		int f_ID = 
		try {
			// JDBC-Treiber laden
			Class.forName(driver);
			// Verbindung aufbauen
			Connection con;
			con = DriverManager.getConnection(url, user, password);
			// SQL-Anweisungen ausfuehren
			Statement stmt = con.createStatement();
			stmt.execute("INSERT INTO T_Votes VALUES (" + f_ID + "," + f_Name + "," + genre1 + "," + f_Name1 + ")");
			con.close();
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();

		}
	}

}