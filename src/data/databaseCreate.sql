CREATE DATABASE datenbankPlaylist;

USE datenbankPlaylist;

CREATE TABLE T_Person
(
    p_Name	VARCHAR (100) NOT NULL,
    Stamm	VARCHAR (50),
    PRIMARY KEY(p_Name)
    );

CREATE TABLE T_Song
(
    p_ID		INT NOT NULL AUTO_INCREMENT,
    Titel		VARCHAR (50),
    Bandname	VARCHAR (50),
    Genre		VARCHAR (50),
    f_Name      VARCHAR (100) NOT NULL,
	PRIMARY KEY(p_ID),
	FOREIGN KEY (f_Name)
           REFERENCES T_Person(p_Name)
    );

CREATE TABLE T_Votes
(
    p_id	INT NOT NULL AUTO_INCREMENT,
	f_ID 	INT NOT NULL,
	f_Name 	VARCHAR (100) NOT NULL,
	PRIMARY KEY(p_id),
	FOREIGN KEY (f_Name)
		REFERENCES T_Person(p_Name),
	FOREIGN KEY (f_ID)
		REFERENCES T_Song(p_ID)
    );