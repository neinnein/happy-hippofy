package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.CardLayout;
import java.awt.Color;

import javax.swing.JButton;
import java.awt.FlowLayout;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JCheckBox;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import javax.swing.BoxLayout;
import java.awt.GridBagConstraints;
import javax.swing.JLabel;
import java.awt.Insets;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JTable;
import javax.swing.JScrollBar;
import java.awt.Font;
import javax.swing.JScrollPane;
import javax.swing.table.DefaultTableModel;

import logic.Benutzer;
import logic.HH_Logic;
import logic.Playlist;
import logic.Songs;
import logic.Voting;
import javax.swing.JList;
import java.awt.ScrollPane;

public class Gui extends JFrame {

	private JPanel contentPane;
	private JTextField txtBenutzer;
	private JTextField txtBandname;
	private JTextField txtTitel;
	private JTextField txtGenre;
	//private HH_Logic hlogic = new HH_Logic();
	private Voting v = new Voting();	
	private JTextField txtAdel;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Gui frame = new Gui();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Gui() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 450);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new CardLayout(0, 0));
		
		JPanel Login = new JPanel();
		contentPane.add(Login, "first_card");
		Login.setLayout(new BorderLayout(0, 0));
		
		JButton btnLogin = new JButton("Login");
		btnLogin.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent a) {
				((CardLayout)contentPane.getLayout()).show(contentPane, "second_card");
			}
		});
		Login.add(btnLogin, BorderLayout.SOUTH);
		
		JPanel panel = new JPanel();
		Login.add(panel, BorderLayout.NORTH);
		
		JLabel lblName = new JLabel("Name");
		
		txtBenutzer = new JTextField();
		txtBenutzer.setColumns(10);
		
		txtAdel = new JTextField();
		txtAdel.setColumns(10);
		
		JLabel lblAdel = new JLabel("Adel");
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.TRAILING)
				.addGroup(Alignment.LEADING, gl_panel.createSequentialGroup()
					.addGap(113)
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING, false)
						.addComponent(lblAdel)
						.addComponent(lblName, GroupLayout.PREFERRED_SIZE, 126, GroupLayout.PREFERRED_SIZE)
						.addComponent(txtBenutzer, GroupLayout.DEFAULT_SIZE, 177, Short.MAX_VALUE)
						.addComponent(txtAdel))
					.addContainerGap(134, Short.MAX_VALUE))
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addGap(112)
					.addComponent(lblName, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(txtBenutzer, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(28)
					.addComponent(lblAdel)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(txtAdel, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(138))
		);
		panel.setLayout(gl_panel);
		
		JPanel panel_4 = new JPanel();
		Login.add(panel_4, BorderLayout.CENTER);
		panel_4.setLayout(new BorderLayout(0, 0));
		
		JPanel Song = new JPanel();
		contentPane.add(Song, "second_card");
		Song.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_1 = new JPanel();
		Song.add(panel_1, BorderLayout.SOUTH);
		
		JButton btnHinzufügen = new JButton("Hinzuf\u00FCgen");
		btnHinzufügen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String b = txtBandname.getSelectedText();
				String t = txtTitel.getSelectedText();
				String g = txtGenre.getSelectedText();
				
			}
		});
		panel_1.add(btnHinzufügen);
		
		JButton btnPlaylist = new JButton("Playlist");
		btnPlaylist.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				((CardLayout)contentPane.getLayout()).show(contentPane, "third_card");
			}
		});
		
		JButton btnVoting = new JButton("Voting");
		btnVoting.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				((CardLayout)contentPane.getLayout()).show(contentPane, "cardfour");
			}
		});
		panel_1.add(btnVoting);
		panel_1.add(btnPlaylist);
		
		JButton btnLogOut = new JButton("LogOut");
		btnLogOut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				((CardLayout)contentPane.getLayout()).show(contentPane, "first_card");	
			}
		});
		panel_1.add(btnLogOut);
		
		JPanel panel_2 = new JPanel();
		Song.add(panel_2, BorderLayout.CENTER);
		panel_2.setLayout(new GridLayout(0, 2, 0, 0));
		
		JLabel lblBandname = new JLabel("                           Bandname:");
		panel_2.add(lblBandname);
		
		txtBandname = new JTextField();
		panel_2.add(txtBandname);
		txtBandname.setColumns(10);
		
		JLabel lblNewLabel_5 = new JLabel("");
		panel_2.add(lblNewLabel_5);
		
		JLabel lblNewLabel_6 = new JLabel("");
		panel_2.add(lblNewLabel_6);
		
		JLabel lblTitel = new JLabel("                           Titel:");
		panel_2.add(lblTitel);
		
		txtTitel = new JTextField();
		panel_2.add(txtTitel);
		txtTitel.setColumns(10);
		
		JLabel lblNewLabel_3 = new JLabel("");
		panel_2.add(lblNewLabel_3);
		
		JLabel lblNewLabel_4 = new JLabel("");
		panel_2.add(lblNewLabel_4);
		
		JLabel lblGenre = new JLabel("                           Genre:");
		panel_2.add(lblGenre);
		
		txtGenre = new JTextField();
		panel_2.add(txtGenre);
		txtGenre.setColumns(10);
		
		JPanel Voting = new JPanel();
		contentPane.add(Voting, "cardfour");
		Voting.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_5 = new JPanel();
		Voting.add(panel_5, BorderLayout.SOUTH);
		
		JButton btnNewButton_2 = new JButton("Hinzuf\u00FCgen");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				((CardLayout)contentPane.getLayout()).show(contentPane, "second_card");
				
			}
		});
		panel_5.add(btnNewButton_2);
		
		JButton btnVote = new JButton("Vote");
		btnVote.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				v.gevotet();
			}
		});
		panel_5.add(btnVote);
		
		JButton btnNewButton_3 = new JButton("Playlist");
		btnNewButton_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				((CardLayout)contentPane.getLayout()).show(contentPane, "third_card");
			}
		});
		panel_5.add(btnNewButton_3);
		
		JButton btnNewButton = new JButton("Logout");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				((CardLayout)contentPane.getLayout()).show(contentPane, "first_card");
			}
		});
		panel_5.add(btnNewButton);
		
		ScrollPane scrollPane_1 = new ScrollPane();
		Voting.add(scrollPane_1, BorderLayout.CENTER);
		
		
		
		JPanel List = new JPanel();
		contentPane.add(List, "third_card");
		List.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_3 = new JPanel();
		List.add(panel_3, BorderLayout.SOUTH);
		
		JButton btnRequest = new JButton("Hinzuf\u00FCgen\r\n");
		btnRequest.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				((CardLayout)contentPane.getLayout()).show(contentPane, "second_card");
			}
		});
		panel_3.add(btnRequest);
		
		JButton btnVoting_2 = new JButton("Voting");
		btnVoting_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				((CardLayout)contentPane.getLayout()).show(contentPane, "cardfour");
			}
		});
		panel_3.add(btnVoting_2);
		
		JButton btnLogOut_2 = new JButton("LogOut");
		btnLogOut_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				((CardLayout)contentPane.getLayout()).show(contentPane, "first_card");
			}
		});
		panel_3.add(btnLogOut_2);
		
		ScrollPane scrollPane = new ScrollPane();
		List.add(scrollPane, BorderLayout.CENTER);
	}
}
